from flask import Flask, render_template
from flask_assets import Bundle, Environment
from flask_caching import Cache

app = Flask(__name__)
cache = Cache(app, config={'CACHE_TYPE':'simple'})

css = Bundle('css/font-awesome.min.css', 
			 'css/fonts.css', 
			 'css/grid12.css', 
			 'css/main.css',
			 filters='cssmin',
			 output='css/min.css')

assets = Environment(app)

assets.register('main_css', css)

@app.route("/")
@cache.cached(timeout=50)
def index():
    return render_template("index.html")

@app.route("/about")
@cache.cached(timeout=50)
def about():
    return render_template("about.html")

if __name__ == "__main__":
    app.run(debug=True)